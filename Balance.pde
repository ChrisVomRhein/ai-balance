final float WORLD_GRAVITY = 0.02;
final float WORLD_DRAG = 1;

final int POPULATION_SIZE = 1000;
final int BRAIN_SIZE = 1000;
final float MUTATION_RATE = 0.05;
final float MAX_PUSH_POWER = 10.0;

float bestFitness = -1;
float worstFitnessEver = -1;
float bestAverageFitnessEver = -1;
int longestAliveEver = -1;
int warnings = 0;
int bestPlayersFound = 0;

int render = 1;

Hud hud;
EvolutionManager evolutionManager;
Population population;

void setup()
{
  size(1000, 600);
  population = new Population(POPULATION_SIZE);
  evolutionManager = new EvolutionManager(MUTATION_RATE);
  hud = new Hud(evolutionManager);
}

void draw()
{  
  background(255);

  if (population.allDead())
  {
    population.calculateFitnessStats();
    log();
    population = evolutionManager.evolve(population);
  }

  population.update();
  hud.update();
}

void keyPressed()
{
  if (key == 'r')
  {
    render ++;

    if (render > 2)
    {
      render = 0;
    }
  }
}

void log()
{
  if (worstFitnessEver == -1 || population.worstFitness < worstFitnessEver)
  {
    worstFitnessEver = population.worstFitness;
  }

  if (population.bestFitness > bestFitness)
  {
    bestFitness = population.bestFitness;
  }

  if (population.averageFitness > bestAverageFitnessEver)
  {
    bestAverageFitnessEver = population.averageFitness;
  }

  if (population.ticksAlive > longestAliveEver)
  {
    longestAliveEver = population.ticksAlive;
  }

  print("===============================\n");
  print("===============================\n");
  print("Generation:\t\t" + evolutionManager.getGeneration() + "\n");
  print("===============================\n");
  print("Ticks Alive:\t\t" + population.ticksAlive + "\n");
  print("Most Ticks Alive:\t" + longestAliveEver + "\n");
  print("===============================\n");

  if (population.bestFitness < bestFitness)
  {
    print("RED ALERT! RED ALERT! RED ALERT! - BEST PLAYER GOT WORSE by " + (bestFitness - population.bestFitness) + "!!!\n");
    bestFitness = population.bestFitness;
    warnings++;
  }

  print("Best Fitness:\t\t" + population.bestFitness + "\n");
  print("Best Fitness Ever:\t" + bestFitness + "\n");
  print("===============================\n");
  print("Worst Fitness:\t\t" + population.worstFitness + "\n");
  print("Worst Fitness Ever:\t" + worstFitnessEver + "\n");
  print("===============================\n");
  print("Average Fitness:\t" + population.averageFitness + "\n");
  print("Best Average Fitness:\t" + bestAverageFitnessEver + "\n");
  print("===============================\n");

  if (population.bestPlayer != 0)
  {
    print("NEW BEST PLAYER!!!\n");
    bestPlayersFound++;
  }

  print("Best Player:\t\t" + population.bestPlayer + "\n");

  if (evolutionManager.getGeneration() > 1)
  {
    print("Prev. Champ Fitness:\t" + population.players[0].fitness + "\n");
  }

  print("===============================\n");
  print("Brain Mutations:\t" + evolutionManager.mutations + "\n");
  print("===============================\n");
  print("Warnings:\t\t" + warnings + "\n");
  print("Best Players Found:\t" + bestPlayersFound + "\n");  

  print("\n");
}
