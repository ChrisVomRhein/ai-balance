class Ball
{
  static final int BALL_RADIUS = 5;
  
  Player player;
    
  PVector pos;
  PVector vel;
  PVector acc;
  
  Ball(Player player, float x, float y)
  {
    this.player = player;
    
    pos = new PVector(x, y);
    vel = new PVector(0, 0);
    acc = new PVector(0, WORLD_GRAVITY);
  }
  
  void update()
  {
    if (player.alive || player.best)
    {
      PVector playerPos = new PVector(player.xPos, player.yPos);
      
      vel.add(acc);
      pos.add(vel);
      
      PVector towardBall = PVector.sub(pos, playerPos);
      float towardBallMag = towardBall.mag();
      towardBall.mult(Player.BALL_DISTANCE / towardBallMag);
      pos = PVector.add(playerPos, towardBall);
    }
  }
  
  void show()
  {
    int c = player.getColor();
    
    fill(c);
    stroke(c);
    
    ellipseMode(RADIUS);
    ellipse(pos.x, pos.y, BALL_RADIUS, BALL_RADIUS);
  }
}
