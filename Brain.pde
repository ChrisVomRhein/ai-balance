enum Push
{
  LEFT,
  RIGHT,
  PAUSE
}

class Brain
{
  float[] push;
  int step;
  
  Brain(int size)
  {
    step = 0;

    push = new float[size];
    
    for (int i = 0; i < push.length; i++)
    {
      push[i] = randomPush();
    }
  }

  Brain(float[] push)
  {
    step = 0;
    this.push = push;
  }

  int size()
  {
    return push.length;
  }

  Brain clone()
  {
    float[] clonedPush = new float[push.length];
    arrayCopy(push, clonedPush);
    return new Brain(clonedPush);
  }

  void mutateAt(int pos)
  {
    if (pos < push.length)
    {
      push[pos] = randomPush();
    }
  }
  
  float next()
  {
    if (step >= push.length)
    {
      step = 0;
    }    
    
    float p = push[step];
    step++;
    return p;
  }

  float randomPush()
  {
    return random(-MAX_PUSH_POWER, MAX_PUSH_POWER);
  }
}
