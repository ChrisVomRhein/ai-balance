class EvolutionManager
{
  Population[] history;
  float mutationRate;
  int generation;
  int mutations = 0;

  EvolutionManager(float mutationRate)
  {
    history = new Population[0];
    this.mutationRate = mutationRate;
    generation = 1;
  }

  int getGeneration()
  {
    return generation;//history.length + 1;
  }

  Population evolve(Population population)
  {
    //history = (Population[])append(history, population);
    generation++;
    mutations = 0;

    int size = population.players.length;

    Player[] nextGeneration = new Player[size];

    for (int i = 1; i < size; i++)
    {
      Player parent = getParent(population);
      Player baby = getBaby(parent);

      nextGeneration[i] = baby;
    }

    Player bestPlayer = population.getBestPlayer().clone();
    bestPlayer.best = true;
    nextGeneration[0] = bestPlayer;

    return new Population(nextGeneration);
  }

  Player getParent(Population population)
  {
    float rand = random(population.totalFitness);
    int size = population.players.length;

    float sum = 0;

    for (int i = 0; i < size; i++)
    {
      sum += population.players[i].fitness;

      if (sum > rand)
      {
        return population.players[i];
      }
    }

    return population.players[size - 1];
  }

  Player getBaby(Player parent)
  {
    Player baby = parent.clone();
    mutate(baby);
    return baby;
  }

  void mutate(Player player)
  {
    Brain brain = player.brain;

    for (int i = 0; i < brain.size(); i++)
    {
      if (random(1) < mutationRate)
      {
        mutations++;
        brain.mutateAt(i);
      }
    }
  }
}