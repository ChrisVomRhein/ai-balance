class Hud
{
  final int FONT_SIZE = 12;

  EvolutionManager evolutionManager;

  Hud(EvolutionManager evolutionManager)
  {
    this.evolutionManager = evolutionManager;
  }
  
  void update()
  {
    textSize(FONT_SIZE);
    fill(0);

    String r = "Nothing";

    if (render == 1)
    {
      r = "Best";
    }
    else if (render == 2)
    {
      r = "All";
    }
    
    String s = "Framerate: " + frameRate + "\n";
    s += "Generation: " + evolutionManager.getGeneration() + "\n";
    s += "Rendermode [R]: " + r + "\n";

    text(s, FONT_SIZE, 2 * FONT_SIZE);
  }
}
