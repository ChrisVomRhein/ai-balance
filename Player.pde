class Player
{
  static final int COLOR_ALIVE = #000000;
  static final int COLOR_DEAD = #888888;
  static final int COLOR_BEST = #00FF00;
  
  static final int WIDTH = 40;
  static final int HEIGHT = 10;
  
  static final int BALL_DISTANCE = 100;
  static final int BALL_OFFSET = -3;
  
  Ball ball;
  Brain brain;
  
  float xPos;
  float yPos;
  float xVel;
  float xAcc;

  float fitness;
  
  boolean alive = true;
  boolean best = false;

  Player()
  {
    xPos = width / 2;
    yPos = 0.8 * height;
    xVel = 0;
    xAcc = 0;

    fitness = 0;
    
    ball = new Ball(this, xPos + BALL_OFFSET, yPos - BALL_DISTANCE);
  }
  
  Player(int brainSize)
  {
    this();
    brain = new Brain(brainSize);
  }

  Player(Brain brain)
  {
    this();
    this.brain = brain;
  }

  Player clone()
  {
    Brain clonedBrain = brain.clone();
    Player clone = new Player(clonedBrain);
    return clone;
  }
  
  void update(Population population)
  {
    if (alive)
    {
      if(ball.pos.y - 2 * Ball.BALL_RADIUS > yPos + HEIGHT)
      {
        die(population);
        return;
      }
      
      
      push(brain.next());
      xVel += xAcc;
      xAcc = 0;
      
      if (abs(xVel) - WORLD_DRAG <= 0)
      {
        xVel = 0;
      }
      if (xVel > 0)
      {
        xVel -= WORLD_DRAG;
      }
      else if (xVel < 0)
      {
        xVel += WORLD_DRAG;
      }
      
      xPos += xVel;
      
      if (xPos <= WIDTH / 2)
      {
        xPos = WIDTH / 2;
        xVel = 0;
      }
      else if (xPos >= width - WIDTH / 2)
      {
        xPos = width - WIDTH / 2;
        xVel = 0;
      }
    }
    
    ball.update();
    
    calculateFitness();
  }

  void calculateFitness()
  {
    PVector towardBall = PVector.sub(ball.pos, new PVector(xPos, yPos));
    float cos = cos(PVector.angleBetween(new PVector(0, -1), towardBall));
    fitness += 10 + cos;
  }

  void push(float power)
  {
    xAcc = power;
  }
  
  int getColor()
  {
    return best ? COLOR_BEST : (alive ? COLOR_ALIVE : COLOR_DEAD);
  }
  
  void show()
  {
    if (alive)
    {
      int c = getColor();
      
      fill(c);
      stroke(c);
      
      rectMode(CENTER);
      rect(xPos, yPos, WIDTH, HEIGHT);
      
      line(ball.pos.x, ball.pos.y, xPos, yPos);
      
      ball.show();
    }
  }
  
  void die(Population population)
  {
    alive = false;
    population.reportDeath();
  }
}
