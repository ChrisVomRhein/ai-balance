class Population
{
  Player[] players;
  int alive;
  int ticksAlive;

  float totalFitness;
  float bestFitness;
  float worstFitness;
  float averageFitness;
  int bestPlayer;

  Population()
  {
    totalFitness = -1;
    bestFitness = -1;
    worstFitness = -1;
    averageFitness = -1;
    bestPlayer = -1;
  }
  
  Population(int size)
  {
    this();

    players = new Player[size];
    alive = size;
    
    for (int i = 0; i < players.length; i++)
    {
      players[i] = new Player(BRAIN_SIZE);
    }
  }

  Population(Player[] nextGeneration)
  {
    this();

    players = nextGeneration;
    alive = nextGeneration.length;
  }
  
  void update()
  {
    ticksAlive++;

    for (int i = players.length - 1; i >= 0; i--)
    {
      players[i].update(this);

      if (render == 2)
      {
        players[i].show();
      }
    }

    if (render == 1)
    {
      players[0].show();
    }
  }

  void reportDeath()
  {
    alive--;
  }

  boolean allDead()
  {
    return alive <= 0;
  }

  void calculateFitnessStats()
  {
    totalFitness = 0;

    for (int i = 0; i < players.length; i++)
    {
      totalFitness += players[i].fitness;

      if(players[i].fitness >= bestFitness)
      {
        bestFitness = players[i].fitness;
        bestPlayer = i;
      }

      if(worstFitness == -1 || players[i].fitness <= worstFitness)
      {
        worstFitness = players[i].fitness;
      }
    }

    averageFitness = totalFitness / players.length;
  }

  Player getBestPlayer()
  {
    if (bestPlayer < 0 || bestPlayer > players.length)
    {
      return null;
    }

    return players[bestPlayer];
  }
}
